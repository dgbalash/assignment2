/*
 *  PolyModel.cpp
 *
 *  Created by Robert Falk on 4/1/10.
 * 
 *  Modified by David Balash on 9/14/14.
 *
 */

#include "PolyModel.h"

#include <vector>
#include <list>
#include <iostream>
#include <math.h>

#ifdef __APPLE__
#include <OpenGL/OpenGL.h>
#include <Glut/glut.h>
#else
#include "stdafx.h"
#include <GL/glut.h>
#endif

using namespace std;

#define NUMBER_OF_VERTICES_IN_AN_EDGE 2
#define NUMBER_OF_VERTICES_IN_A_TRIANGLE 3
#define PI 3.14159265

/**
 * The draw method will draw the poly model.
 */
void PolyModel::draw()
{
    
    if (collisionDetected)
    {
        glColor3f(0.435294, 0.258824, 0.258824);
    }
    
    // Draw each triangle...
    glBegin(GL_TRIANGLES);
    for( int i = 0; i < m_polys.size(); i++)
    {        
        for (int j = 0; j < NUMBER_OF_VERTICES_IN_A_TRIANGLE; j++)
            glVertex3fv(m_verts[m_polys[i][j]].getPtr());
        
    }
    glEnd();
    
    // Draw the center point.
    glBegin( GL_POINTS );
        glVertex2f(m_center.x(), m_center.y());
    glEnd();
   
    // Add a new for loop
	for ()
	{
	}

 
    // Draw the edges that are involved in a collision.
    glColor3f(0.85, 0.85, 0.10);
    glLineWidth(5);
    for( int i = 0; i < m_touching_edges.size(); i++)
    {
        glBegin(GL_LINES);
        for (int j = 0; j < NUMBER_OF_VERTICES_IN_AN_EDGE; j++)
            glVertex2fv(m_verts[m_touching_edges[i][j]].getPtr());
        glEnd();
    }
    for( int i = 0; i < m_touching_edges2.size(); i++)
    {
        glBegin(GL_LINES);
        for (int j = 0; j < NUMBER_OF_VERTICES_IN_AN_EDGE; j++)
            glVertex2fv(m_verts[m_touching_edges2[i][j]].getPtr());
        glEnd();
    }
    glLineWidth(1);
}

void PolyModel::setCollisionDetected(bool detected)
{
    collisionDetected = detected;
}

float PolyModel::getObjectAngle()
{
    return objectAngle;
}

/**
 * The draw circle method will draw a circle around the poly model.
 */
void PolyModel::drawCircle()
{
    if (collisionDetected)
    {
        glColor3f(1, 1, 0);
        glLineWidth(5);
    }
    else
    {
        glColor3f(0, 0, 0);
    }
    
    glBegin(GL_LINE_LOOP);
        // for each 10 degrees draw a new line
        for (int angle = 0; angle < 360; angle += 10)
        {
            glVertex2f(m_center.x() + sin(degreesToRadians(angle)) * modelRadius,
                       m_center.y() + cos(degreesToRadians(angle)) * modelRadius);
        }
    glEnd();
    glLineWidth(1);
}


/**
 * A helper method to convert degrees to radians.
 */
float PolyModel::degreesToRadians(float thetaDegrees)
{
    return thetaDegrees * PI / 180.0;
}

/**
 * A rotate method to rotate the m_verts by the input angle theta (degrees).
 */
void PolyModel::rotate(float theta)
{
    // Convert theta degrees to radians.
    theta = degreesToRadians(theta);
    
    // For each vertex rotate by angle theta.
    for (int i = 0; i < m_verts.size() ; i++)
    {
        m_verts[i].x() = m_verts[i].x() - m_center.x();
        m_verts[i].y() = m_verts[i].y() - m_center.y();
        
        float x = m_verts[i].x();
        float y = m_verts[i].y();
        
        m_verts[i].x() = (x * cos(theta)) - (y * sin(theta));
        m_verts[i].y() = (x * sin(theta)) + (y * cos(theta));
        
        m_verts[i].x() = m_verts[i].x() + m_center.x();
        m_verts[i].y() = m_verts[i].y() + m_center.y();
    }
}


/**
 * A translate method to translate the m_verts by the input.
 */
void PolyModel::translate(Vec3f directionVector, float scalarTranslateDistance)
{
    // For each vertex translate by x and y.
    for (int i = 0; i < m_verts.size() ; i++)
    {        
        m_verts[i].x() = m_verts[i].x() + (directionVector.x() * scalarTranslateDistance);
        m_verts[i].y() = m_verts[i].y() + (directionVector.y() * scalarTranslateDistance);
    }
    
    // recalculate center
    calculateCenter();
    calculateRadius();
}


float dotProduct(Vec3f u, Vec3f v)
{
    return
          (u.x() * v.x())
        + (u.y() * v.y())
        + (u.z() * v.z());
}

float getProjectionDistance(Vec3f v, Vec3f u, Vec3f point)
{
    float dist = v.distance(u);
    
    // distance squared , saving a sqrt step.
    float distSquared = dist * dist;
    
    if (distSquared == 0.0)
    {
        return point.distance(v);
    }
    
    float t = dotProduct(point - v, u - v) / distSquared;
    
    // V vector end
    if (t < 0.0)
    {
        return point.distance(v);
    }
    // u vector end
    else if (t > 1.0)
    {
        return point.distance(u);
    }
    
    // Return the projection
    Vec3f projection = (v + ((u - v) * t));
    
    return point.distance(projection);
}



float PolyModel::detectCollision(Vec3f centerOfShip, float radius)
{
    float minDistance = 0;
    
    m_touching_edges.clear();
    m_touching_edges.resize(m_external_edges.size());
    
    int numberOfTouchingEdges = 0;
    
    for( int i = 0; i < m_external_edges.size(); i++)
    {
        if (m_external_edges[i][2] == 1)
        {
            Vec3f linePoint1 = m_verts[m_external_edges[i][0]];
            Vec3f linePoint2 = m_verts[m_external_edges[i][1]];
            
            float minDistanceResult = getProjectionDistance(linePoint1, linePoint2, centerOfShip);
            if (minDistanceResult < radius)
            {
                m_touching_edges[numberOfTouchingEdges] = m_external_edges[i];
                numberOfTouchingEdges++;
                
                if (minDistanceResult > minDistance)
                {
                    minDistance = minDistanceResult;
                    
                    Vec3f edgeDirection = linePoint2 - linePoint1;
                    //cos theta = (u.v)/(|u||v|) angle between two vectors formula.
                    float thetaRadians = acosf(Vec3f(1,0,0).dotProduct(edgeDirection) / edgeDirection.length());
                    objectAngle = thetaRadians * 180.0 / PI;
                    break;
                }
            }
        }
    }
    
    m_touching_edges.resize(numberOfTouchingEdges);
    
    
    if (minDistance > 0)
    {
        return radius - minDistance;
    }
    
    return 0;
}


float PolyModel::detectCollision2(Vec3f centerOfShip, float radius)
{
    float minDistance = 0;
    
    m_touching_edges2.clear();
    m_touching_edges2.resize(m_external_edges.size());
    
    int numberOfTouchingEdges = 0;
    
    for( int i = 0; i < m_external_edges.size(); i++)
    {
        if (m_external_edges[i][2] == 1)
        {
            Vec3f linePoint1 = m_verts[m_external_edges[i][0]];
            Vec3f linePoint2 = m_verts[m_external_edges[i][1]];
            
            float minDistanceResult = getProjectionDistance(linePoint1, linePoint2, centerOfShip);
            if (minDistanceResult < radius)
            {
                m_touching_edges2[numberOfTouchingEdges] = m_external_edges[i];
                numberOfTouchingEdges++;
                
                if (minDistanceResult > minDistance && numberOfTouchingEdges > 1)
                {
                    minDistance = minDistanceResult;
                    
                    Vec3f edgeDirection = linePoint2 - linePoint1;
                    //cos theta = (u.v)/(|u||v|) angle between two vectors formula.
                    float thetaRadians = acosf(Vec3f(1,0,0).dotProduct(edgeDirection) / edgeDirection.length());
                    objectAngle = thetaRadians * 180.0 / PI;
                }
            }
        }
    }
    
    m_touching_edges2.resize(numberOfTouchingEdges);
    
    
    if (minDistance > 0)
    {
        return radius - minDistance;
    }
    
    return 0;
}



/**
 * The load model method will load the poly model from a input stream.
 */
bool PolyModel::loadModel(std::istream& istr, bool reverse)
{
	int vertex_count;
	int face_count;
	string data;
	
	if (!istr.good())
		return false;
	
    // data number-of-vertices number-of-triangles named-point-attributes(1..n - optional)
    istr >> data >> vertex_count >> face_count;
	
	m_verts.resize(vertex_count);
	m_polys.resize(face_count);
    
    cout << "Vertex count = " << vertex_count << "\n";
    cout << "Face count = " << face_count << "\n";
    
    // Read in all the points
    // <all points as: x y z>
    for (int i = 0; i < vertex_count; i++)
    {
        Vec3f v;
        istr >> v.x() >> v.y() >> v.z();
        m_verts[i] = v;
    }

    int externalEdgeCount = 0;
    
    // Read in all the polygons (triangles)
    // <all triangles as: 3 point-idx1 point-idx2 point-idx3>
    for (int i = 0; i < face_count; i++)
    {
        vector<int> triangleVertices(3);
        int numVertices;
        istr >> numVertices >> triangleVertices[0] >> triangleVertices[1] >> triangleVertices[2];
        m_polys[i] = triangleVertices;
        
        m_external_edges.resize(face_count * 3);
        
        if (externalEdgeCount == 0)
        {
            vector<int> edge1(2);
            edge1[0] = triangleVertices[0];
            edge1[1] = triangleVertices[1];
            
            vector<int> edge2(2);
            edge2[0] = triangleVertices[1];
            edge2[1] = triangleVertices[2];
            
            m_external_edges[0] = edge1;
            m_external_edges[1] = edge2;
            
            externalEdgeCount += 2;
        }
        else
        {
            vector<int> edge1(2);
            edge1[0] = triangleVertices[0];
            edge1[1] = triangleVertices[1];
            
            bool containedInExternalEdges = false;
            for (int j = 0; j < externalEdgeCount; j++)
            {
                if ((m_external_edges[j][0] == edge1[0] && m_external_edges[j][1] == edge1[1])
                    ||
                    (m_external_edges[j][0] == edge1[1] && m_external_edges[j][1] == edge1[0]))
                {
                    containedInExternalEdges = true;
                    m_external_edges[j][2] = 2;
                    break;
                }
            }
            
            if (!containedInExternalEdges)
            {
                m_external_edges[externalEdgeCount] = edge1;
                m_external_edges[externalEdgeCount][2] = 1;
                externalEdgeCount++;
            }
            
            vector<int> edge2(2);
            edge2[0] = triangleVertices[1];
            edge2[1] = triangleVertices[2];
            
            containedInExternalEdges = false;
            for (int j = 0; j < externalEdgeCount; j++)
            {
                if ((m_external_edges[j][0] == edge2[0] && m_external_edges[j][1] == edge2[1])
                    ||
                    (m_external_edges[j][0] == edge2[1] && m_external_edges[j][1] == edge2[0]))
                {
                    containedInExternalEdges = true;
                    m_external_edges[j][2] = 2;
                    break;
                }
            }
            
            if (!containedInExternalEdges)
            {
                m_external_edges[externalEdgeCount] = edge2;
                m_external_edges[externalEdgeCount][2] = 1;
                externalEdgeCount++;
            }
            
            vector<int> edge3(2);
            edge3[0] = triangleVertices[2];
            edge3[1] = triangleVertices[0];
            
            containedInExternalEdges = false;
            for (int j = 0; j < externalEdgeCount; j++)
            {
                if ((m_external_edges[j][0] == edge3[0] && m_external_edges[j][1] == edge3[1])
                    ||
                    (m_external_edges[j][0] == edge3[1] && m_external_edges[j][1] == edge3[0]))
                {
                    containedInExternalEdges = true;
                    m_external_edges[j][2] = 2;
                    break;
                }
            }
            
            if (!containedInExternalEdges)
            {
                m_external_edges[externalEdgeCount] = edge3;
                m_external_edges[externalEdgeCount][2] = 1;
                externalEdgeCount++;
            }
        }
        
        
        
        m_external_edges.resize(externalEdgeCount);
    }
    
    calculateCenter();
    calculateRadius();
    
    collisionDetected = false;
    
	return true;
}

void PolyModel::calculateCenter()
{
    float xMin = numeric_limits<float>::max();
    float xMax = numeric_limits<float>::min();
    float yMin = numeric_limits<float>::max();
    float yMax = numeric_limits<float>::min();


    for (int i = 0; i < m_verts.size(); i++)
    {
        if (m_verts[i].x() > xMax)
        {
            xMax = m_verts[i].x();
        }
        
        if (m_verts[i].x() < xMin)
        {
            xMin = m_verts[i].x();
        }
        
        if (m_verts[i].y() > yMax)
        {
            yMax = m_verts[i].y();
        }
        
        if (m_verts[i].y() < yMin)
        {
            yMin = m_verts[i].y();
        }
    }
    
    float deltaX = xMax - xMin;
    float deltaY = yMax - yMin;
    
	// Calculate and set the m_center for this polymodel.
    m_center.x() = ((deltaX) / 2.0) + xMin;
    m_center.y() = ((deltaY) / 2.0) + yMin;
}


void PolyModel::calculateRadius()
{
    modelRadius = 0;
    for (int i = 0; i < m_verts.size(); i++)
    {
        float distanceFromCenter = m_verts[i].distance(m_center);
        
        if (distanceFromCenter > modelRadius)
        {
            modelRadius = distanceFromCenter;
        }
    }
}
