/*
 *  PolyModel.h
 *
 *  Created by Robert Falk on 4/1/10.
 *
 *  Modified by David Balash on 9/14/14.
 *  Add new comment.
 */

#pragma once

#include <vector>
#include <iostream>
#include <math.h>

using namespace std;

class Vec3f
{
public:
    // constructors..
	Vec3f() {};
	Vec3f(float x, float y, float z) { m_vec[0]=x; m_vec[1]=y; m_vec[2] = z; }
	
	// Comparison operators...
	bool operator==(const Vec3f& rhs) const { return (m_vec[0]==rhs.m_vec[0] && m_vec[1]==rhs.m_vec[1] && m_vec[2]==rhs.m_vec[2]); }
    
	// Math operators (+,-,/,*,+=,-=,/=,*=,negation...)
    
    // Here are some examples for vector addition:
    
	Vec3f& operator+=(const Vec3f& rhs);
	Vec3f& operator*=(float scalar);
	Vec3f operator+(const Vec3f& rhs) const;
    Vec3f operator-(const Vec3f& rhs) const;
	Vec3f operator*(float scalar) const;
    Vec3f operator*(const Vec3f& rhs) const;
    Vec3f operator-() const;
	
	// functions: dotProduct(), crossProduct(), length(), normalize()
    float dotProduct(const Vec3f& vec3f) const;
    Vec3f cross(const Vec3f& vec3f) const;
    float length() const;
    Vec3f& normalize();
    float distance(const Vec3f& vec3f) const;
    
    
	// Set/get values
	float& x() { return m_vec[0]; }
	float x() const { return m_vec[0]; }
	float& y() { return m_vec[1]; }
	float y() const { return m_vec[1]; }
	float& z() { return m_vec[2]; }
	float z() const { return m_vec[2]; }
	
	float* getPtr() { return m_vec; }
	const float* getPtr() const { return m_vec; }
	
protected:
    
	float m_vec[3];
};

inline Vec3f Vec3f::operator-() const
{
    return Vec3f(-m_vec[0], -m_vec[1], -m_vec[2]);
}

inline Vec3f Vec3f::operator-(const Vec3f& rhs) const
{
    return Vec3f(m_vec[0] - rhs.x(), m_vec[1] - rhs.y(), m_vec[2] - rhs.z());
}

inline Vec3f Vec3f::operator+(const Vec3f& rhs) const
{
    return Vec3f(m_vec[0] + rhs.x(), m_vec[1] + rhs.y(), m_vec[2] + rhs.z());
}

inline float Vec3f::length() const
{
    return sqrtf(
                 (m_vec[0] * m_vec[0])
                 + (m_vec[1] * m_vec[1])
                 + (m_vec[2] * m_vec[2])
                 );
}


inline float Vec3f::dotProduct(const Vec3f &vec3f) const
{
    return
    (m_vec[0] * vec3f.x())
    + (m_vec[1] * vec3f.y())
    + (m_vec[2] * vec3f.z());
}


inline Vec3f Vec3f::operator*(const float scalar) const
{
    return Vec3f(m_vec[0] * scalar, m_vec[1] * scalar, m_vec[2] * scalar);
}

inline Vec3f Vec3f::operator*(const Vec3f& rhs) const
{
    return Vec3f(m_vec[0] * rhs.x(), m_vec[1] * rhs.y(), m_vec[2] * rhs.z());
}

inline float Vec3f::distance(const Vec3f& vec3f) const
{
    return
    sqrtf(
          ((vec3f.x() - m_vec[0]) * (vec3f.x() - m_vec[0]))
          + ((vec3f.y() - m_vec[1]) * (vec3f.y() - m_vec[1]))
          + ((vec3f.z() - m_vec[2]) * (vec3f.z() - m_vec[2]))
          );
}


class PolyModel
{
public:
	PolyModel() {}
	
	void draw();
    void drawCircle();
    void calculateCenter();
    void calculateRadius();
    float detectCollision(Vec3f centerOfShip, float radius);
    float detectCollision2(Vec3f centerOfShip, float radius);
    float getObjectAngle();
    void setCollisionDetected(bool detected);

    void rotate(float theta);
    void translate(Vec3f directionVector, float scalarTranslateDistance);
    
    bool loadModel(istream& istr, bool reverse=false);
    
    float getModelRadius() const { return modelRadius; }
    Vec3f getCenter() const { return m_center; }
    Vec3f getMinBounding() const { return m_min_bounding; }
    Vec3f getMaxBounding() const { return m_max_bounding; }


protected:
    
    // A degrees to radians helper method.
    float degreesToRadians(float thetaDegrees);
    
    // All the points in the model
	vector<Vec3f> m_verts;
    
    // All the triangles in the model
	vector< vector<int> > m_polys;
    
    // All the external edges in the model
	vector< vector<int> > m_external_edges;
    
    // All the edges the ship is touching in the model
	vector< vector<int> > m_touching_edges;
    
    // All the edges the ship is touching in the model
	vector< vector<int> > m_touching_edges2;
    
    Vec3f m_center;
    Vec3f m_min_bounding;
    Vec3f m_max_bounding;
    float modelRadius;
    bool collisionDetected;
    float objectAngle;
};


