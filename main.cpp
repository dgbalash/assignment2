/**********************************************************************
 *
 *  Modified by David Balash on 9/14/14.
 *
 *  CS 4554 assignment 2
 *  Fall 2014
 *
 In this project you will use the PolyModel class you created for the first project to load both a background (gameboard) and a movable object (gamepiece) to move around the gameboard.  You will move the gamepiece with keyboard controls.  Please use the following controls (so that I don't have to learn everybody's personal keyboard setup!):
 
 w -> Move forward (in the direction the gamepiece is pointing)
 s -> Move backward
 a -> Move sideways to the left (relative to the direction you are pointing)
 d -> Move sideways to the right.
 Use the left and right arrow keys to change the direction you are pointing.
 Left Arrow -> Rotate the gamepiece counterclockwise
 Right Arrow -> Rotate the gamepiece clockwise
 This will allow you to move all over the gameboard. Lastly, check each time you move the gamepiece to make sure it does not collide with any elements of the gameboard.  If it does stop it or let it slide along the thing it hit without penetrating it. This last feature, collisions, will be worth 10 points with 5 for detecting the collision and 5 for the quality of the collision response.  If your code implements collision detection without response show that a collision is occurring visually be drawing the gamepiece in a different color.
 *  
 *  
 **********************************************************************/

#ifdef __APPLE__
#include <OpenGL/OpenGL.h>
#include <Glut/glut.h>
#else
#include "stdafx.h"
#include <GL/glut.h>
#endif
#include <math.h>
#include <iostream>
#include <fstream>
#include "PolyModel.h"


using namespace std;

/* ascii codes for special keys */
#define ESCAPE 27

/**********************************************************************
 * Configuration
 **********************************************************************/

#define INITIAL_WIDTH 600
#define INITIAL_HEIGHT 600
#define INITIAL_X_POS 100
#define INITIAL_Y_POS 100
#define PI 3.14159265

#define WINDOW_NAME     "Assignment 2"

// Change this to the directory where you have the .d2 files on your
// computer, or put the files in your programs default start up directory.
#define RESOURCE_DIR   "/Users/david/Documents/George Washington University/Computer Graphics 1/Assignment2/"

 /**********************************************************************
 * Globals
 **********************************************************************/

GLsizei window_width;
GLsizei window_height;

// You need to implment a class to load the model and draw it to the screen
// using OpenGL.  You don't have to use the class I gave you as a starting
// point if you don't want to.
PolyModel backgroundModel;
PolyModel gamePieceModel;

// The initial ship direction is the unit vector 0, 1, 0.
Vec3f shipDirection(0,1,0);

int shipTheta = 90;


/**********************************************************************
 * Set the new size of the window
 **********************************************************************/

void resize_scene(GLsizei width, GLsizei height)
{
    glViewport(0, 0, width, height);  /* reset the current viewport and 
                                       * perspective transformation */
    window_width  = width;
    window_height = height;

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    
    /* For the purposes of this assignment, we want the world coordinate
     * system to match the dimensions of the models to be displayed..*/
    gluOrtho2D(0, 100, 0, 100);
    glMatrixMode(GL_MODELVIEW);
}

/**********************************************************************
 * any program initialization - set initial OpenGL state
 **********************************************************************/
void init()
{
    cout << WINDOW_NAME << "\n";
    cout << "David Balash 2014\n";
    
    // Load any models you will need during initialization
    string backgroundFile = RESOURCE_DIR + string("background.d2");
    ifstream backgroundIfstream(backgroundFile.c_str());
    cout << "Loading module " << backgroundFile << "\n";
    backgroundModel.loadModel(backgroundIfstream);

    string gamePieceFile = RESOURCE_DIR + string("gamepiece.d2");
    ifstream gamePieceIfstream(gamePieceFile.c_str());
    cout << "Loading module " << gamePieceFile << "\n";
    gamePieceModel.loadModel(gamePieceIfstream);
 
    // Set any OpenGL options that will not change during the program
    glDisable(GL_LIGHTING);
    glDisable(GL_CULL_FACE);
}

/**********************************************************************
 * The main drawing functions. 
 **********************************************************************/
void draw_scene(void)
{
    /* clear the screen and the depth buffer */
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    /* reset modelview matrix */
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    
    // Draw game background
    glColor3f(1.0f, 0.0f, 0.0f);
    backgroundModel.draw();
    
    // Draw game piece
    glColor3f(0.0f, 0.0f, 1.0f);
    gamePieceModel.draw();
    gamePieceModel.drawCircle();
   
    /* since this is double buffered, swap the
     * buffers to display what just got drawn */
    glutSwapBuffers();
}

/**********************************************************************
 * the update function for this project - this function will be called 
 * every few milliseconds and will then set a timer to call itself again. 
 * The first call to update() happens when you press the space bar
 **********************************************************************/
void update(int game)
{
    /* tell glut when to call this function next  */
    glutTimerFunc(30, update, 1);
    

    
    /* tell glut when to call display again to redraw */
    glutPostRedisplay();
}


Vec3f crossProduct(Vec3f u, Vec3f v)
{
    Vec3f crossProduct;
    crossProduct.x() = (u.y() * v.z()) - (u.z() * v.y());
    crossProduct.y() = (u.z() * v.x()) - (u.x() * v.z());
    crossProduct.z() = (u.x() * v.y()) + (u.y() * v.x());
    return crossProduct;
}

float degreesToRadians(float thetaDegrees)
{
    return thetaDegrees * PI / 180.0;
}

float radiansToDegrees(float thetaRadians)
{
    return thetaRadians * 180.0 / PI;
}

/**********************************************************************
 * this function is called whenever a key is pressed
 **********************************************************************/
void key_press(unsigned char key, int x, int y) 
{
    float moveSize = 0.7;
    
    Vec3f zUnitVector(0, 0, 1);
    Vec3f leftDirection = crossProduct(zUnitVector, shipDirection);
    Vec3f rightDirection = crossProduct(shipDirection, zUnitVector);
    Vec3f reverseDirection = -shipDirection;
    gamePieceModel.setCollisionDetected(false);
    
    float collisionDistance = 0.0;
    switch (key)
    {
        // Space bar starts the updating
        case ' ':
            update(1);
            break;
            
        // Up direction
        case 'w':
            gamePieceModel.translate(shipDirection, moveSize);
            collisionDistance = backgroundModel.detectCollision(gamePieceModel.getCenter(), gamePieceModel.getModelRadius());
            
            if (collisionDistance > 0)
            {
                gamePieceModel.setCollisionDetected(true);
             
                float objectAngle = backgroundModel.getObjectAngle();
                
                gamePieceModel.translate(-shipDirection, collisionDistance);
                
                if (objectAngle > 60  && objectAngle < 120)
                {
                    if (shipTheta >= 0 && shipTheta <= 180)
                    {
                        gamePieceModel.translate(Vec3f(0, 1, 0), moveSize);
                    }
                    if (shipTheta > 180 && shipTheta <= 360)
                    {
                        gamePieceModel.translate(-Vec3f(0, 1, 0), moveSize);
                    }
                }
                else
                {
                    if ((shipTheta >= 0 && shipTheta <= 90) || (shipTheta >= 270 && shipTheta <= 360))
                    {
                        gamePieceModel.translate(Vec3f(1, 0, 0), moveSize);
                    }
                    if (shipTheta > 90 && shipTheta < 270)
                    {
                        gamePieceModel.translate(-Vec3f(1, 0, 0), moveSize);
                    }
                }
            }
            
            
            collisionDistance = backgroundModel.detectCollision2(gamePieceModel.getCenter(), gamePieceModel.getModelRadius());
            
            if (collisionDistance > 0)
            {
                gamePieceModel.setCollisionDetected(true);
                
                float objectAngle = backgroundModel.getObjectAngle();
                
                gamePieceModel.translate(-shipDirection, collisionDistance);
                
                if (objectAngle > 60  && objectAngle < 120)
                {
                    if (shipTheta >= 0 && shipTheta <= 180)
                    {
                        gamePieceModel.translate(Vec3f(0, 1, 0), moveSize);
                    }
                    if (shipTheta > 180 && shipTheta <= 360)
                    {
                        gamePieceModel.translate(-Vec3f(0, 1, 0), moveSize);
                    }
                }
                else
                {
                    if ((shipTheta >= 0 && shipTheta <= 90) || (shipTheta >= 270 && shipTheta <= 360))
                    {
                        gamePieceModel.translate(Vec3f(1, 0, 0), moveSize);
                    }
                    if (shipTheta > 90 && shipTheta < 270)
                    {
                        gamePieceModel.translate(-Vec3f(1, 0, 0), moveSize);
                    }
                }
            }
            
            break;
            
        // Left direction
        case 'a':
            gamePieceModel.translate(leftDirection, moveSize);
            collisionDistance = backgroundModel.detectCollision(gamePieceModel.getCenter(), gamePieceModel.getModelRadius());
            
            if (collisionDistance > 0)
            {
                gamePieceModel.setCollisionDetected(true);
                
                float objectAngle = backgroundModel.getObjectAngle();
                
                gamePieceModel.translate(rightDirection, collisionDistance);
                
                if (objectAngle > 60  && objectAngle < 120)
                {
                    if (shipTheta >= 0 && shipTheta <= 180)
                    {
                        gamePieceModel.translate(Vec3f(0, 1, 0), moveSize);
                    }
                    if (shipTheta > 180 && shipTheta <= 360)
                    {
                        gamePieceModel.translate(-Vec3f(0, 1, 0), moveSize);
                    }
                }
                else
                {
                    if ((shipTheta >= 0 && shipTheta <= 90) || (shipTheta >= 270 && shipTheta <= 360) || shipTheta == 0)
                    {
                        gamePieceModel.translate(Vec3f(1, 0, 0), moveSize);
                    }
                    if (shipTheta > 90 && shipTheta < 270)
                    {
                        gamePieceModel.translate(-Vec3f(1, 0, 0), moveSize);
                    }
                }
            }
            
            collisionDistance = backgroundModel.detectCollision2(gamePieceModel.getCenter(), gamePieceModel.getModelRadius());
            
            if (collisionDistance > 0)
            {
                gamePieceModel.setCollisionDetected(true);
                
                float objectAngle = backgroundModel.getObjectAngle();
                
                gamePieceModel.translate(rightDirection, collisionDistance);
                
                if (objectAngle > 60  && objectAngle < 120)
                {
                    if (shipTheta >= 0 && shipTheta <= 180)
                    {
                        gamePieceModel.translate(Vec3f(0, 1, 0), moveSize);
                    }
                    if (shipTheta > 180 && shipTheta <= 360)
                    {
                        gamePieceModel.translate(-Vec3f(0, 1, 0), moveSize);
                    }
                }
                else
                {
                    if ((shipTheta >= 0 && shipTheta <= 90) || (shipTheta >= 270 && shipTheta <= 360) || shipTheta == 0)
                    {
                        gamePieceModel.translate(Vec3f(1, 0, 0), moveSize);
                    }
                    if (shipTheta > 90 && shipTheta < 270)
                    {
                        gamePieceModel.translate(-Vec3f(1, 0, 0), moveSize);
                    }
                }
            }
            
            break;
        
        // Down direction
        case 's':
            gamePieceModel.translate(reverseDirection, moveSize);
            collisionDistance = backgroundModel.detectCollision(gamePieceModel.getCenter(), gamePieceModel.getModelRadius());
            
            if (collisionDistance > 0)
            {
                gamePieceModel.setCollisionDetected(true);
                
                float objectAngle = backgroundModel.getObjectAngle();
                
                gamePieceModel.translate(shipDirection, collisionDistance);
                
                
                if (objectAngle > 60  && objectAngle < 120)
                {
                    if (shipTheta >= 0 && shipTheta <= 180)
                    {
                        gamePieceModel.translate(Vec3f(0, 1, 0), moveSize);
                    }
                    if (shipTheta > 180 && shipTheta <= 360)
                    {
                        gamePieceModel.translate(-Vec3f(0, 1, 0), moveSize);
                    }
                }
                else
                {
                    if ((shipTheta >= 0 && shipTheta <= 90) || (shipTheta >= 270 && shipTheta <= 360))
                    {
                        gamePieceModel.translate(Vec3f(1, 0, 0), moveSize);
                    }
                    if (shipTheta > 90 && shipTheta < 270)
                    {
                        gamePieceModel.translate(-Vec3f(1, 0, 0), moveSize);
                    }
                }
            }
            
            collisionDistance = backgroundModel.detectCollision2(gamePieceModel.getCenter(), gamePieceModel.getModelRadius());
            
            if (collisionDistance > 0)
            {
                gamePieceModel.setCollisionDetected(true);
                
                float objectAngle = backgroundModel.getObjectAngle();
                
                gamePieceModel.translate(shipDirection, collisionDistance);
                
                
                if (objectAngle > 60  && objectAngle < 120)
                {
                    if (shipTheta >= 0 && shipTheta <= 180)
                    {
                        gamePieceModel.translate(Vec3f(0, 1, 0), moveSize);
                    }
                    if (shipTheta > 180 && shipTheta <= 360)
                    {
                        gamePieceModel.translate(-Vec3f(0, 1, 0), moveSize);
                    }
                }
                else
                {
                    if ((shipTheta >= 0 && shipTheta <= 90) || (shipTheta >= 270 && shipTheta <= 360))
                    {
                        gamePieceModel.translate(Vec3f(1, 0, 0), moveSize);
                    }
                    if (shipTheta > 90 && shipTheta < 270)
                    {
                        gamePieceModel.translate(-Vec3f(1, 0, 0), moveSize);
                    }
                }
            }
            
            break;
            
        // Right direction
        case 'd':
            gamePieceModel.translate(rightDirection, moveSize);
            collisionDistance = backgroundModel.detectCollision(gamePieceModel.getCenter(), gamePieceModel.getModelRadius());
           
            if (collisionDistance > 0)
            {
                gamePieceModel.setCollisionDetected(true);
                
                float objectAngle = backgroundModel.getObjectAngle();
                
                gamePieceModel.translate(leftDirection, collisionDistance);
                
                if (objectAngle > 60  && objectAngle < 120)
                {
                    if (shipTheta >= 0 && shipTheta <= 180)
                    {
                        gamePieceModel.translate(Vec3f(0, 1, 0), moveSize);
                    }
                    if (shipTheta > 180 && shipTheta <= 360)
                    {
                        gamePieceModel.translate(-Vec3f(0, 1, 0), moveSize);
                    }
                }
                else
                {
                    if ((shipTheta >= 0 && shipTheta <= 90) || (shipTheta >= 270 && shipTheta <= 360) || shipTheta == 0)
                    {
                        gamePieceModel.translate(Vec3f(1, 0, 0), moveSize);
                    }
                    if (shipTheta > 90 && shipTheta < 270)
                    {
                        gamePieceModel.translate(-Vec3f(1, 0, 0), moveSize);
                    }
                }
            }
            
            collisionDistance = backgroundModel.detectCollision2(gamePieceModel.getCenter(), gamePieceModel.getModelRadius());
            
            if (collisionDistance > 0)
            {
                gamePieceModel.setCollisionDetected(true);
                
                float objectAngle = backgroundModel.getObjectAngle();
                
                gamePieceModel.translate(leftDirection, collisionDistance);
                
                if (objectAngle > 60  && objectAngle < 120)
                {
                    if (shipTheta >= 0 && shipTheta <= 180)
                    {
                        gamePieceModel.translate(Vec3f(0, 1, 0), moveSize);
                    }
                    if (shipTheta > 180 && shipTheta <= 360)
                    {
                        gamePieceModel.translate(-Vec3f(0, 1, 0), moveSize);
                    }
                }
                else
                {
                    if ((shipTheta >= 0 && shipTheta <= 90) || (shipTheta >= 270 && shipTheta <= 360) || shipTheta == 0)
                    {
                        gamePieceModel.translate(Vec3f(1, 0, 0), moveSize);
                    }
                    if (shipTheta > 90 && shipTheta < 270)
                    {
                        gamePieceModel.translate(-Vec3f(1, 0, 0), moveSize);
                    }
                }
            }
            
            break;
            
        // exit the program
        case ESCAPE:
            exit(0);
            
        default:
            break;
    }
}

/**********************************************************************
 * this function is called whenever the mouse is moved
 **********************************************************************/

void handle_mouse_motion(int x, int y)
{
    // If you do something here that causes a change to what the user
    // would see, call glutPostRedisplay to force a redraw
    //glutPostRedisplay();
}

/**********************************************************************
 * this function is called whenever a mouse button is pressed or released
 **********************************************************************/

void handle_mouse_click(int btn, int state, int x, int y)
{
    switch (btn)
    {
        case GLUT_LEFT_BUTTON:
            break;
    }
}


/**
 * A rotate method to rotate a vector by the input angle theta (degrees).
 */
void rotateShip(float theta)
{
    // Convert theta degrees to radians.
    theta = degreesToRadians(theta);
    
    float x = shipDirection.x();
    float y = shipDirection.y();
    
    shipDirection.x() = (x * cos(theta)) - (y * sin(theta));
    shipDirection.y() = (x * sin(theta)) + (y * cos(theta));
    
    //cout << shipDirection.x() << " " << shipDirection.y() << "\n";
}

/**********************************************************************
 * this function is called for non-standard keys like up/down/left/right
 * arrows.
 **********************************************************************/
void special_key(int key, int x, int y)
{
    switch (key)
    {
        case GLUT_KEY_RIGHT: //right arrow
            rotateShip(-5);
            gamePieceModel.rotate(-5);
            if (shipTheta > 0)
            {
                shipTheta += -5;
            }
            else
            {
                shipTheta = 355;
            }
            break;
        case GLUT_KEY_LEFT: //left arrow
            rotateShip(5);
            gamePieceModel.rotate(5);
            if (shipTheta < 360)
            {
                shipTheta += 5;
            }
            else
            {
                shipTheta = 5;
            }
            break;
        case GLUT_KEY_UP: //up arrow
            //glutPostRedisplay();
            break;
        case GLUT_KEY_DOWN: //down arrow
            //glutPostRedisplay();
            break;
        default:
            break;
    }
}


int main(int argc, char * argv[]) 
{
  	/* Initialize GLUT */
    glutInit(&argc, argv);  
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);  
    glutInitWindowSize(INITIAL_WIDTH, INITIAL_HEIGHT);  
    glutInitWindowPosition(INITIAL_X_POS, INITIAL_Y_POS);  
    glutCreateWindow(WINDOW_NAME);  

    /* Register callback functions */
	glutDisplayFunc(draw_scene);     
    glutReshapeFunc(resize_scene);       //Initialize the viewport when the window size changes.
    glutKeyboardFunc(key_press);         //handle when the key is pressed
    glutMouseFunc(handle_mouse_click);   //check the Mouse Button(Left, Right and Center) status(Up or Down)
    glutMotionFunc(handle_mouse_motion); //Check the Current mouse position when mouse moves
	glutSpecialFunc(special_key);        //Special Keyboard Key fuction(For Arrow button and F1 to F10 button)

    /* OpenGL and other program initialization */
    init();

    /* Enter event processing loop */
    glutMainLoop();  

    return 1;
}

